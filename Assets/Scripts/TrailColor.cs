﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailColor : MonoBehaviour
{
    TrailRenderer lineRenderer;
    [SerializeField] Color c2;

    private void Start()
    {
        lineRenderer = GetComponent<TrailRenderer>();
    }
    public void SetColor(Color c1)
    {
        if (lineRenderer == null)
        { lineRenderer = GetComponent<TrailRenderer>(); }
        lineRenderer.startColor = c1;
        lineRenderer.endColor = c2;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
