﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int best;
    public int score;
    public int currentStage = 0;
    public int currentPass = 0;
    public bool isDead;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        { Destroy(gameObject); }

        // Load the saved highscore
        best = PlayerPrefs.GetInt("Highscore");
        FloatingTextController.Initialize();
    }

    public void AddScore(int scoreToAdd)
    {
        score += scoreToAdd;

        if (score > best)
        {
            PlayerPrefs.SetInt("Highscore", score);
            best = score;
        }
    }


}
