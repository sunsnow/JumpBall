﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeSceneManger : MonoBehaviour
{
    public Text showtb;
    public Vector3 StartTouchPos;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                StartTouchPos = Input.touches[0].position;
            }
            if (Input.touches[0].phase == TouchPhase.Ended)
            {
                float deltaX = Input.touches[0].position.x - StartTouchPos.x;
                float deltaY = Input.touches[0].position.y - StartTouchPos.y;
                if (Mathf.Abs(deltaX) > Mathf.Abs(deltaY))
                {
                    if (deltaX > 0)
                    { showtb.text = "right"; }
                    else
                    { showtb.text = "left"; }
                }
                else
                {
                     if (deltaY > 0)
                    { showtb.text = "up"; }
                    else
                    { showtb.text = "down"; }
                }

            }
        }
    }
}
