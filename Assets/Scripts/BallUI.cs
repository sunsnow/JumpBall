﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class BallUI : MonoBehaviour
{
    public Text score;
    public Text best;
    public Transform AddScore;
    [Header("Level UI")]
    public HelixController helixController;
    public Image process;
    public Text currentStage;
    public Text nextStage;
    public float t = 0.5f;
    float processTarget;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (helixController == null)
            helixController = FindObjectOfType<HelixController>();
        if (score != null)
            score.text = GameManager.Instance.score.ToString();
        if (best != null)
            best.text = "BEST：" + GameManager.Instance.best.ToString();
        if (currentStage != null)
            currentStage.text = (GameManager.Instance.currentStage + 1).ToString();
        if (nextStage != null)
            nextStage.text = (GameManager.Instance.currentStage + 2).ToString();
        if (process != null)
        {
            processTarget = (float)GameManager.Instance.currentPass / (helixController.allStages[0].levels.Count + 1);//GameManager.Instance.currentStage
            process.fillAmount = Mathf.Lerp(process.fillAmount, processTarget, t);
        }

    }
    public void NextLevel()
    {
        GameManager.Instance.currentStage++;
        GameManager.Instance.score = 0;
        GameManager.Instance.currentPass = 0;
        if (GameManager.Instance.currentStage < SceneManager.sceneCountInBuildSettings)
        { }
        else
        { GameManager.Instance.currentStage = 0; }
        SceneManager.LoadScene(GameManager.Instance.currentStage);
        FindObjectOfType<BallController>().ResetBall();
        FindObjectOfType<HelixController>().LoadStage(GameManager.Instance.currentStage);
    }

    public void Restart()
    {
        // Show Adds Advertisement.Show();
        GameManager.Instance.score = 0;
        GameManager.Instance.currentPass = 0;
        FindObjectOfType<BallController>().ResetBall();
        FindObjectOfType<HelixController>().LoadStage(GameManager.Instance.currentStage);
    }
}
