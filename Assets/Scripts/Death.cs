﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<Renderer>().material.color = 
        FindObjectOfType<HelixController>().allStages[0].stageDeathPartColor;//GameManager.Instance.currentStage
        gameObject.tag = "Death";
    }

    public void HittedDeathPart()
    {
        //GameManager.Instance.Restart();
    }
    // Start is called before the first frame update
    void Start()
    {

    }
    public void SetColor(Color c1)
    {
        GetComponent<Renderer>().material.color = c1;
    }


    // Update is called once per frame
    void Update()
    {

    }
}
