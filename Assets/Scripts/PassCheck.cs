﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassCheck : MonoBehaviour
{
    public float radius = 5.0F;
    public float power = 10.0F;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            BallController player = FindObjectOfType<BallController>();
            int score = (GameManager.Instance.currentStage + 1) * player.perfectPass + 1;
            FloatingTextController.CreateFloatingText(score.ToString(), FindObjectOfType<BallUI>().AddScore);
            GameManager.Instance.AddScore(score);
            GameManager.Instance.currentPass++;
            player.perfectPass++;
            if (this.tag != "Top")
            { boom(); }
        }
    }
    public void boom()
    {
        Collider[] colliders = GetComponentsInChildren<Collider>();
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
                rb.AddExplosionForce(power, transform.position, radius, 0.0F, ForceMode.Acceleration);
                Destroy(rb.gameObject, 5.0f);
            }
        }
    }
}
