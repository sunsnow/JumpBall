﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BounceBall : MonoBehaviour
{
    public float height = 2;
    public float speed = 1;
    private Rigidbody rb;
    private bool isforce;
    // Start is called before the first frame update
    void Start()
    { rb = GetComponent<Rigidbody>(); }
    void Update()
    {
        if (isforce)
        { force(); }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(Physics.gravity.y * Vector3.up * speed, ForceMode.Acceleration);
    }

    void force()
    {
        isforce = false;
        Vector3 newV = Vector3.up * Mathf.Sqrt(height);
        rb.velocity = newV * Mathf.Abs(speed);
        rb.AddForce(Vector3.up * height , ForceMode.Impulse);
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Ground")
        { isforce = true; }
        // float vy = Mathf.Sqrt(Mathf.Abs(Physics.gravity.y * 2 * 2));
        // Vector3 newV = Vector3.up * vy * Mathf.Sqrt(height);
        // rb.velocity = newV * Mathf.Abs(speed);
    }
}
